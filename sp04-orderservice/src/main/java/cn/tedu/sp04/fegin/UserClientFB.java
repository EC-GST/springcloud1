package cn.tedu.sp04.fegin;


import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.User;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserClientFB implements UserClient{
    @Override
    public JsonResult<User> getUser(Integer userId) {
        // 模拟缓存数据
        if (Math.random() < 0.5) { // 如果有返回缓存数据

            User user=new User(1, "迪丽热巴", "ShineGirl");

            return JsonResult.ok().data(user);
        }
        return JsonResult.err("查询用户失败");
    }

    @Override
    public JsonResult<?> addScore(Integer userId, Integer score) {
        return JsonResult.err("添加积分失败");
    }
}
