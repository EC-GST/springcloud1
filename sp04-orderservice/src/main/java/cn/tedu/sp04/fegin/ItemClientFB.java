package cn.tedu.sp04.fegin;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ItemClientFB implements ItemClient {


    @Override
    public JsonResult<List<Item>> getItems(String orderId) {
        // 模拟缓存数据
        if (Math.random() < 0.5) { // 如果有返回缓存数据
            List<Item> list = new ArrayList<>();
            list.add(new Item(1, "商品1", 8));
            list.add(new Item(2, "商品2", 8));
            list.add(new Item(3, "商品3", 8));
            list.add(new Item(4, "商品4", 8));
            list.add(new Item(5, "商品5", 8));
            return JsonResult.ok().data(list);
        }


        return JsonResult.err("调用商品失败");
    }

    @Override
    public JsonResult<?> decreaseNumber(List<Item> items) {

        return JsonResult.err("减少商品库存失败");
    }
}
