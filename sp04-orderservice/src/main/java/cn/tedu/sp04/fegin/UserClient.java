package cn.tedu.sp04.fegin;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.User;
import cn.tedu.web.util.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  调用商品服务的声明式客户端接口
 */
@FeignClient(name = "user-service",fallback = UserClientFB.class)  //  指定调用的是哪个服务
public interface UserClient {
    @GetMapping("/{userId}")
    JsonResult<User> getUser(@PathVariable Integer userId);
    @GetMapping("/{userId}/score")
    JsonResult<?> addScore(@PathVariable Integer userId, @RequestParam Integer score);
}
