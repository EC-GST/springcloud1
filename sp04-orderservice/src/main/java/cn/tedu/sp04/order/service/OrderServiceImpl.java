package cn.tedu.sp04.order.service;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.Order;
import cn.tedu.sp01.pojo.User;
import cn.tedu.sp01.service.OrderService;
import cn.tedu.sp04.fegin.ItemClient;
import cn.tedu.sp04.fegin.UserClient;
import cn.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {
    @Autowired
    private ItemClient itemClient;

    @Autowired
    private UserClient userClient;
    @Override
    public Order getOrder(String orderId) {
        log.info("获取订单 ：orderId="+orderId);
        //TODO: 调用user-service获取用户信息
        JsonResult<User> user = userClient.getUser(8);

        //TODO: 调用item-service获取商品信息
        JsonResult<List<Item>> item=itemClient.getItems(orderId);

        Order order = new Order();
        order.setId(orderId);
        order.setUser(user.getData());
        order.setItems(item.getData());


        return order;
    }

    @Override
    public void saveOrder(Order order) {
        log.info("保存订单："+order);

        //TODO: 调用item-service减少商品库存
        itemClient.decreaseNumber(order.getItems());
        //TODO: 调用user-service增加用户积分
        userClient.addScore(order.getUser().getId(),200);
    }
}
