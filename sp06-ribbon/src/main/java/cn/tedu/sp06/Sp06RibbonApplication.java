package cn.tedu.sp06;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ScheduledExecutorService;

@SpringBootApplication
@EnableCircuitBreaker   //
public class Sp06RibbonApplication {

    public static void main(String[] args) {

        SpringApplication.run(Sp06RibbonApplication.class, args);

    }

    @Bean
    @LoadBalanced   //ribbon增强resttmplate
    public RestTemplate restTemplate() {
        // 请求工厂设置超时时间
       SimpleClientHttpRequestFactory factory= new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(1000);
        factory.setReadTimeout(1000);
        return new RestTemplate(factory);
    }
}
