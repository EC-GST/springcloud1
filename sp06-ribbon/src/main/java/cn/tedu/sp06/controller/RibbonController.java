package cn.tedu.sp06.controller;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.Order;
import cn.tedu.sp01.pojo.User;
import cn.tedu.web.util.JsonResult;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@Slf4j

public class RibbonController {
    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "getItemsFB")  //  降级方法名
    @GetMapping("/item-service/{orderId}")
    public JsonResult<Item> getItems(@PathVariable String orderId) {
        //  远程调用 商品服务  查询列表  {1} 占位符  值为最后的
        return restTemplate.getForObject("http://item-service/{1}", JsonResult.class, orderId);

    }

    @HystrixCommand(fallbackMethod = "decreaseNumberFB")  //  降级方法名
    @PostMapping("/item-service/decreaseNumber")
    public JsonResult<?> decreaseNumber(@RequestBody List<Item> items) {

        return restTemplate.postForObject("http://item-service/decreaseNumber", items, JsonResult.class);
    }


    @HystrixCommand(fallbackMethod = "getUserFB")  //  降级方法名
    @GetMapping("/user-service/{userId}")
    public JsonResult<User> getUser(@PathVariable Integer userId) {
        return restTemplate.getForObject("http://user-service/{1}", JsonResult.class, userId);
    }


    @HystrixCommand(fallbackMethod = "addScoreFB")  //  降级方法名
    @GetMapping("/user-service/{userId}/score")
    public JsonResult<User> addScore(@PathVariable Integer userId, Integer score) {
        return restTemplate.getForObject("http://user-service/{1}/score?score={2}",
                JsonResult.class, userId, score);

    }

    //订单
    @HystrixCommand(fallbackMethod = "getOrderFB")  //  降级方法名
    @GetMapping("/order-service/{orderId}")
    public JsonResult<Order> getOrder(@PathVariable String orderId) {
        return restTemplate.getForObject("http://order-service/{1}", JsonResult.class, orderId);
    }

    @HystrixCommand(fallbackMethod = "saveOrderFB")  //  降级方法名
    @GetMapping("/order-service")
    public JsonResult<?> saveOrder() {
        return restTemplate.getForObject("http://order-service", JsonResult.class);
    }




    //--------------------------------------------------------------------------
    public JsonResult<Item> getItemsFB(@PathVariable String orderId) {
        return JsonResult.err().msg("调用商品出错了");

    }

    public JsonResult<?> decreaseNumberFB(@RequestBody List<Item> items) {
        return JsonResult.err().msg("调用商品出错了");
    }

    public JsonResult<User> getUserFB(@PathVariable Integer userId) {
        return JsonResult.err().msg("调用用户出错了");
    }


    public JsonResult<User> addScoreFB(@PathVariable Integer userId, Integer score) {
        return JsonResult.err().msg("调用用户出错了");
    }

    public JsonResult<Order> getOrderFB(@PathVariable String orderId) {
        return JsonResult.err().msg("调用订单出错了");
    }

    public JsonResult<?> saveOrderFB() {
        return JsonResult.err().msg("调用订单出错了");
    }
}