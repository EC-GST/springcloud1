package cn.tedu.sp09.controller;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.Order;
import cn.tedu.sp01.pojo.User;
import cn.tedu.sp09.fegin.ItemClient;
import cn.tedu.sp09.fegin.OrderClient;
import cn.tedu.sp09.fegin.UserClient;
import cn.tedu.web.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FeginController {

    @Autowired
    private ItemClient itemClient;
    @Autowired
    private UserClient userClient;
    @Autowired
    private OrderClient orderClient;


    @GetMapping("/item-service/{orderId}")
    public JsonResult<List<Item>> getItem(@PathVariable String orderId){
        return itemClient.getItems(orderId);
    }
    @PostMapping("/item-service/decreaseNumber")
    public JsonResult<?> decreaseNumber(@RequestBody List<Item> items){
        return itemClient.decreaseNumber(items).msg("减少商品库存了+++");
    }


    @GetMapping("/user-service/{userId}")
    public JsonResult<User> getUser(@PathVariable Integer userId){
        return userClient.getUser(userId);
    }
    @GetMapping("/user-service/{userId}/score")
    public JsonResult<?> addScore(@PathVariable Integer userId,@RequestParam Integer score){
        return userClient.addScore(userId, score);
    }
    @GetMapping("/order-service/{orderId}")
    public JsonResult<Order> getUser(@PathVariable String orderId){
        return orderClient.getOrder(orderId);
    }

    @GetMapping("/order-service")
    public JsonResult<?> saveOrder(){
        return orderClient.saveOrder();
    }
}
