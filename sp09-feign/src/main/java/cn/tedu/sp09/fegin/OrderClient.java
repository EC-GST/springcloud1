package cn.tedu.sp09.fegin;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.Order;
import cn.tedu.web.util.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  调用商品服务的声明式客户端接口
 */
@FeignClient(name = "order-service",fallback = OrderClientFB.class)  //  指定调用的是哪个服务
public interface OrderClient {
    @GetMapping("/{orderId}")
    JsonResult<Order> getOrder(@PathVariable String orderId);
    @GetMapping("/")
    JsonResult<?> saveOrder();
}
