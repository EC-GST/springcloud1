package cn.tedu.sp09.fegin;

import cn.tedu.sp01.pojo.Order;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

@Component
public class OrderClientFB implements OrderClient {
    @Override
    public JsonResult<Order> getOrder(String orderId) {
        return JsonResult.err("调用订单失败");
    }

    @Override
    public JsonResult<?> saveOrder() {
        return JsonResult.err("保存订单失败");
    }
}
