package cn.tedu.sp09.fegin;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.web.util.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 *  调用商品服务的声明式客户端接口
 */
@FeignClient(name = "item-service",fallback = ItemClientFB.class)  //  指定调用的是哪个服务  降级类
public interface ItemClient {
    @GetMapping("/{orderId}")
    JsonResult<List<Item>> getItems(@PathVariable String orderId);
    @PostMapping("/decreaseNumber")
    JsonResult<?> decreaseNumber(@RequestBody  List<Item> items);
}

