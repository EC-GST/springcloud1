package cn.tedu.sp05;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer   // 表示为eureka 自动配置eureka
public class Sp05EurekaApplication {

    public static void main(String[] args) {

        SpringApplication.run(
                Sp05EurekaApplication.class, args);
    }

}
