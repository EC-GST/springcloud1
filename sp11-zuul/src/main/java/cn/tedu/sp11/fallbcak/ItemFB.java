package cn.tedu.sp11.fallbcak;

import cn.tedu.web.util.JsonResult;
import com.netflix.appinfo.RefreshableAmazonInfoProvider;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Component
public class ItemFB implements FallbackProvider {

    /* 设置当前降级类 针对哪个服务降级
       item-service : 针对商品服务降级
       "*" 相等 null ：对当前所有服务都降级
     */
    @Override
    public String getRoute() {
        return "item-service";
    }

    @Override
    //  向客户端发送降级响应
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {

        return new ClientHttpResponse() {
            @Override
            public HttpStatus getStatusCode() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR;
            }

            @Override
            public int getRawStatusCode() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR.value();
            }

            @Override
            public String getStatusText() throws IOException {
                return HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
            }

            @Override
            public void close() {
                // 响应结束 关闭输入流
            }

            @Override
            public InputStream getBody() throws IOException {
                // {code:500,msg:"调用商品服务失败,data:null"}
                String json = JsonResult.err().code(500).msg("调用商品服务失败").toString();
                return new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8));
            }

            @Override
            public HttpHeaders getHeaders() {
                HttpHeaders h =new HttpHeaders();
                h.add("Content-Type", "application/json;charset=UTF-8");
                return h;
            }
        };
    }
}
