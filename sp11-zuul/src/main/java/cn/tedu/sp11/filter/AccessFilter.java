package cn.tedu.sp11.filter;

import cn.tedu.web.util.JsonResult;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang.StringUtils;
import org.apache.http.protocol.RequestContent;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AccessFilter extends ZuulFilter {

    @Override
    //过滤器类型：pre , routing , post , error ...
    public String filterType() {

        return FilterConstants.PRE_TYPE;


    }

    @Override
    //  过滤器添加的位置  顺序号
    public int filterOrder() {
        return 6;  // 前五个是有默认的   所以从6开始
    }

    @Override
    /*
     * 判断对当前请求，是否执行过滤代码
     * 如果是用户调用商品，执行过滤
     * 如果是调用其他服务，不执行
     */
    public boolean shouldFilter() {
        // 获取用户调用服务的id  是item-service就是true

        RequestContext rec = RequestContext.getCurrentContext();
        String serviceId = (String) rec.get(FilterConstants.SERVICE_ID_KEY);
        // equalsIgnoreCase 忽略大小写比较
        return "item-service".equalsIgnoreCase(serviceId);
    }

    @Override
    //  过滤代码
    public Object run() throws ZuulException {
        //接受token参数  http://localhost:3001/item-service/46y?token=456465
        //如果没有token ，直接返回登录提示
        // 获取上下文 在根据上下文 获取request对象
        RequestContext rec = RequestContext.getCurrentContext();
        HttpServletRequest request = rec.getRequest();
        String token = request.getParameter("token");
        if (StringUtils.isBlank(token)){
            // token 为空  阻止继续调用服务
            rec.setSendZuulResponse(false);
            // 返回登录提示
            rec.setResponseStatusCode(401);
            rec.addZuulResponseHeader("Content-Type",
                    "application/json;charset=UTF-8");
            rec.setResponseBody(JsonResult.err().code(401).msg("没登录看P的订单！").toString());
            return "";
        }
        return null; // 返回值现在没什么用 为任意即可  以后可能有用
    }
}
