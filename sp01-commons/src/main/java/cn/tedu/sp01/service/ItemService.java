package cn.tedu.sp01.service;

import cn.tedu.sp01.pojo.Item;

import java.util.List;

public interface ItemService {
    // 列表
    List<Item> getItems(String orderId);
    // 减少
    void decreaseNumbers(List<Item> list);
}
