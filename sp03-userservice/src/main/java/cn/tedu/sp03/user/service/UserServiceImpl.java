package cn.tedu.sp03.user.service;


import cn.tedu.sp01.pojo.User;
import cn.tedu.sp01.service.UserService;
import cn.tedu.web.util.JsonUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RefreshScope  //  刷新到新的配置数据  就注入新配置
public class UserServiceImpl implements UserService {

    // yml 配置的属性 注入这个变量
    @Value("${sp.user-service.users}")
    private String userJson;

    @Override
    public User getUser(Integer userId) {
        log.info("id"+userId,userJson+"userJson");
        List<User> list = JsonUtil.from(userJson, new TypeReference<List<User>>(){});
        for (User u : list) {
            if (userId.equals(u.getId()))
                return u;
        }
        return new User(userId,"用户名"+userId,"密码"+userId);
    }

    @Override
    public void addScore(Integer userId, Integer score) {
        log.info("增加用户积分，userId"+userId+". score="+score);
    }
}
