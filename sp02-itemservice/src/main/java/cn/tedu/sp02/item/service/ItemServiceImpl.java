package cn.tedu.sp02.item.service;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.service.ItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ItemServiceImpl implements ItemService {
    @Override
    public List<Item> getItems(String orderId) {
        List<Item> lists= new ArrayList<>();
        lists.add(new Item(1,"商品1",2));
        lists.add(new Item(2,"商品2",5));
        lists.add(new Item(3,"商品3",4));
        lists.add(new Item(4,"商品4",3));
        lists.add(new Item(5,"商品5",2));

        return lists;
    }

    @Override
    public void decreaseNumbers(List<Item> items) {
        for (Item item : items) {
            log.info("减少库存"+items);
        }

    }
}
